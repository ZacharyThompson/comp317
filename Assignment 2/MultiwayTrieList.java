/**
 * Multiway Trie implementation that uses a Priority List as the underlying mechanism
 * to insert and search children. In the case wherein a Trie node may have many
 * children, but certain nodes are disproportionately accessed, this implementation
 * performs reasonably well.
 *
 * @param <K> The type of keys maintained by this Trie.
 * @param <V> The type of elements in this Trie.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class MultiwayTrieList<K, V> extends MultiwayTrie<K, V> {
    /**
     * The PriorityList that contains the children.
     */
    private PriorityList<MultiwayTrie<K, V>> children = new PriorityList<>();

    /**
     * Constructs a Multiway Trie instance with a key, value pair that are mapped to a
     * parent Trie instance.
     *
     * @param key Key of which the specified value is associated with.
     * @param value Value to be associated with the key.
     * @param parent The parent MultiwayTrie node of this instance.
     */
    @SuppressWarnings("unchecked")
    public MultiwayTrieList(K key, V value, MultiwayTrie<K, V> parent) {
        super(key, value, parent);
    }

    /**
     * Constructs a Multiway Trie instance with a key, value pair.
     *
     * @param key Key of which the specified value is associated with.
     * @param value Value to be associated with the key.
     */
    public MultiwayTrieList(K key, V value) {
        this(key, value, null);
    }

    /**
     * Inserts a key, value pair as a child into this Multiway Trie instance.
     *
     * @param key Key of which the specified value is associated with.
     * @param value Value to be associated with the key.
     */
    public void insert(K key, V value) {
        children.insert(new MultiwayTrieList<>(key, value));
    }

    /**
     * Searches for a value associated with a given key.
     *
     * @param key Key of which the target value is associated with.
     * @return The value associated with the given key.
     */
    public MultiwayTrie<K, V> search(K key) {
        return children.search(key);
    }
}
