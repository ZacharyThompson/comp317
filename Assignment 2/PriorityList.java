/**
 * A list implementation that will push the conceptually more important
 * elements to the front of the list. This, in theory, would reduce the lookup
 * time for elements that appear frequently. This implementation has no advantage
 * if the item being searched, however, does not appear in the list an will require
 * and entire iteration to determine the item does not exist.
 *
 * ToDo Handle Integer overflow on priority.
 *
 * @param <E> The type of elements in this list.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class PriorityList<E> {
    /**
     * The head of the list. This will contain the element that is seen as the most important.
     */
    private PriorityNode<E> head;

    /**
     * The tail of the list. This enabled O(1) insertion.
     */
    private PriorityNode<E> tail;

    /**
     * Inserts an element at the end of the list. As the priority of an element never
     * decreases, the last item will always be the most important.
     *
     * @param element The element to be inserted.
     */
    public void insert(E element) {
        PriorityNode<E> node = new PriorityNode<>(element, tail, null);
        if (head == null) {
            head = tail = node;
        } else {
            tail = tail.nextNode = node;
        }
    }

    /**
     * Searches for an element within the list that is equal to an Object.
     *
     * @param o The object to match against an item within the list.
     * @return The element that matches the object, if found, otherwise null.
     */
    public E search(Object o) {
        return (head == null) ? null : head.search(o);
    }

    /**
     * The node structure for the Priority List implementation. It contains a reference
     * to the previous and next node (double linked list), the priority of the node and
     * the element it refers to.
     *
     * @param <E> The type of elements in this list.
     */
    private static class PriorityNode<E> {
        /**
         * The element associated with this Priority node.
         */
        private E element;

        /**
         * The priority of this node.
         */
        private int priority;

        /**
         * The previous node in the list.
         */
        private PriorityNode<E> prevNode;

        /**
         * The next node in the list.
         */
        private PriorityNode<E> nextNode;

        /**
         * Constructs a Priority node for an element by mapping the previous and next nodes.
         *
         * @param element The element associated with this node.
         * @param prevNode The previous node in the list.
         * @param nextNode The next node in the list.
         */
        public PriorityNode(E element, PriorityNode<E> prevNode, PriorityNode<E> nextNode) {
            this.element = element;
            this.prevNode = prevNode;
            this.nextNode = nextNode;
        }

        /**
         * Searches for an element in the list that is equal to the given Object.
         *
         * @param o The item to search for.
         * @return The element that first equals the supplied Object, otherwise null.
         */
        public E search (Object o) {
            if (element.equals(o)) {
                E tempData = element;
                priority++;
                swap();
                return tempData;
            }
            return (nextNode == null) ? null : nextNode.search(o);
        }

        /**
         * Swaps the previous node with the next node. Invoked when the priority of a node
         * is increased to ensure the priority order of the list is maintained.
         */
        private void swap() {
            if (prevNode != null && prevNode.priority < priority) {
                swap(prevNode, this);
                prevNode.swap();
            }
        }

        /**
         * Swaps the contents of two nodes. While swapping by reference is conceptually a
         * better idea, the fact that java is pass by value makes this tedious for this
         * implementation. The PriorityList wrapper classes contains a reference to the
         * head and tail. Swapping the head with the second item in the list by changing
         * the links of the nodes would leave the head and tail of the list unchanged.
         * Swapping the data within the nodes, specifically the priority and element,
         * would achieve the same effect.
         *
         * @param nodeA The first node to swap.
         * @param nodeB The second node to swap.
         * @param <E> The element type stored in the nodes.
         */
        private static <E> void swap(PriorityNode<E> nodeA, PriorityNode<E> nodeB) {
            int tempPriority = nodeA.priority;
            nodeA.priority = nodeB.priority;
            nodeB.priority = tempPriority;

            E tempElement = nodeA.element;
            nodeA.element = nodeB.element;
            nodeB.element = tempElement;
        }
    }


}
