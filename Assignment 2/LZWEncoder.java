import java.io.*;

import static java.lang.System.lineSeparator;

/**
 * A Lempel–Ziv–Welch encoding implementation that encodes an input stream provided.
 *
 * The primary limitation of this implementation is the maxinmum number of bits must be between 8 and 32 (non inclusive).
 *  As the standard symbol set is every possible byte value, 2^8 phrases must be present initially Another phrase number
 *  is reserved for the reset token, hence (2^8 + 1) = 257 phrases, or 9 bits, are required as a minimum.
 *  Working with numbers larger than 2^31 becomes problematic for several reasons:
 * 	 1. You will need either treat the primitive int as unsigned (functionality provided in the Integer wrapper class) or use
 *      long.
 *   2. The maximum size of an array is Java is theoretically Integer.MAX_VALUE (2^31 - 1). However, this varies between implementation
 *      Regardless, a phrase number that would require the most significant bit in an unsigned 32 bit integer would certainly exceed the
 *      maximum size.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class LZWEncoder {
    /**
     * The size of the buffer to be used to read bytes from the input stream to avoid
     * expensive per byte reading.
     */
    private static final int BUFFER_SIZE = 1024;

    /**
     * The current phrase count.
     */
    private int phraseCount;

    /**
     * The dictionary containing the known phrases as interpreted from the encoded input.
     */
    private MultiwayTrie<Byte, Integer> dictionary;

    /**
     * Constructs an encoder that will attempt to encoder a given input stream and output
     * the encoded phrase numbers to the given output stream. The input will be read as a stream
     * of bytes, with a predefined symbol set of all byte values.
     *
     * @param input The source input stream to be encoded.
     * @param output The output stream for which the encoded input should be written to.
     * @param maxBits The maximum number of bits that can be used to describe a phrase number.
     */
    public LZWEncoder(InputStream input, OutputStream output, int maxBits) {
        if (maxBits < 9 || maxBits > 31) {
            System.err.println(
				"The maximum number of bits to encode a phrase number is invalid." +
                lineSeparator() +
                "Encoding cannot be done with less than 9 bits as the initial symbol set contains all byte values (256) and a reset token (1)." +
				lineSeparator() +
                "Encoding cannot be done with greater than 31 bits as this would exceed the maximum array size (~Integer.MAX_VALUE)."
			);
            return;
        }
        // Populate the dictionary.
        resetDictionary();
        try (
				BufferedInputStream bufferedInput = new BufferedInputStream(input);
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output))
        ){
            MultiwayTrie<Byte, Integer> phrase = dictionary;
            int maxLength = (1 << maxBits) - 1, bytes;
            byte[] data = new byte[BUFFER_SIZE];
			boolean growing = true;
            // Read the data into the buffer.
            while ((bytes = bufferedInput.read(data)) >= 0) {
                // Process each byte individually.
                for (int i = 0; i < bytes; i++) {
                    // Find the next byte in the phrase multiway trie.
                    MultiwayTrie<Byte, Integer> tempPhrase = phrase.search(data[i]);
                    // If the phrase was not found, it was a mismatch. Otherwise move the phrase pointer along.
                    if (tempPhrase == null) {
                        // Write the phrase number associated with the longest matching phrase.
                        writer.append(phrase.getValue().toString()).append(lineSeparator());
                        // If a new phrase can fit, add it. Otherwise reset the dictionary.
                        if (growing) {
							if (phraseCount < maxLength) {
								phrase.insert(data[i], phraseCount++);
							} else {
								writer.append('1').append(lineSeparator());
								growing = false;
								//resetDictionary();
							}
						}
                        // Locate the mismatch from the root level.
                        phrase = dictionary.search(data[i]);
                    } else {
                        phrase = tempPhrase;
                    }
                }
				writer.flush();
            }
            // Write the longest matching phrase that has yet to be encoded.
            writer.append(phrase.getValue().toString());
        } catch (IOException e) {
            System.err.println("An IO exception occurred while attempting to encode.");
        }
    }

    /**
     * Resets the dictionary.
     */
    private void resetDictionary() {
        dictionary = new MultiwayTrieArray<>(null, null, 256, 128);
        phraseCount = 2;
        for (int i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; i++) {
            dictionary.insert((byte) i, phraseCount++);
        }
    }

    public static void main (String args[]) {
        if (args.length > 1) {
            System.err.println("Invalid arguments: Expected 'java LZWEncoder [Maximum Bits]");
            System.exit(0);
        }
        int maxBits = 16;
        if (args.length == 1) {
            try {
                maxBits = Integer.parseInt(args[0]);
            } catch (Exception e) {
                System.err.println(String.format("Invalid arguments: %s is not an integer", args[0]));
                System.exit(0);
            }
        }
        LZWEncoder encoder = new LZWEncoder(System.in, System.out, maxBits);
    }
}