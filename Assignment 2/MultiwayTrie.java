/**
 * A Multi-Way Trie abstract implementation that defines standard behaviour
 * for a concrete implementation. The abstract behaviour allows for the underlying
 * insertion and searching to vary significantly between implementations.
 *
 * While some implementations would be appropriate for general purpose use, there
 * are use cases wherein certain implementations would perform exceptionally. Likewise,
 * there are use cases wherein certain implementation would perform extremely poorly.
 *
 * An implementation that used a fixed size array would be appropriate if each layer of the
 * Multi-Way trie was known to have a fixed number of children. However, if the number of children
 * any particular layer of the Multi-Way trie may have is highly volatile, this approach would
 * produced an excessive overhead that could very quickly consume available system resources.
 *
 * @param <K> The type of keys maintained by this Trie.
 * @param <V> The type of elements in this Trie.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public abstract class MultiwayTrie<K, V> {
    /**
     * The key mapped to a value.
     */
    private K key;

    /**
     * The value the key is mapped to.
     */
    private V value;

    /**
     * The parent of this MultiwayTrie node.
     */
    private MultiwayTrie<K, V> parent;

    /**
     * Constructs a MultiwayTrie node from a key, value pair.
     *
     * @param key Key of which the specified value is associated with.
     * @param value Value to be associated with the key.
     */
    public MultiwayTrie(K key, V value) {
        this(key, value, null);
    }

    /**
     * Constructs a MultiwayTrie node from a key, value pair and stores the parent MultiwayTrie node.
     *
     * @param key Key of which the specified value is associated with.
     * @param value Value to be associated with the key.
     * @param parent The parent MultiwayTrie node of this instance.
     */
    public MultiwayTrie(K key, V value, MultiwayTrie<K, V> parent) {
        this.key = key;
        this.value = value;
        this.parent = parent;
    }

    /**
     * Inserts a key, value pair into the underlying children structure of the MultiwayTrie node.
     *
     * @param key Key of which the specified value is associated with.
     * @param value Value to be associated with the key.
     */
    public abstract void insert (K key, V value);

    /**
     * Searches for a value using a given key.
     *
     * @param key Key of which the target value is associated with.
     * @return The value associated with the key.
     */
    public abstract MultiwayTrie<K, V> search (K key);

    /**
     * Gets the key associated with this MultiwayTrie instance.
     *
     * @return The key.
     */
    public K getKey() {
        return key;
    }

    /**
     * Gets the value associated with this MultiwayTrie instance.
     *
     * @return The value.
     */
    public V getValue() {
        return value;
    }

    /**
     * Sets the key associated with this MultiwayTrie instance.
     *
     * @param key The key.
     */
    public void setKey(K key) {
        this.key = key;
    }

    /**
     * Sets the value associated with this MultiwayTrie instance.
     *
     * @param value The value.
     */
    public void setValue(V value) {
        this.value = value;
    }

    /**
     * Gets the parent associated with this MultiwayTrie instance.
     *
     * @return The parent MultiwayTrie node.
     */
    public MultiwayTrie<K, V> getParent() {
        return parent;
    }

    /**
     * Compares the specified object with this map for equality.
     * Uses the partial keys to evaluate equality if the parameter is a MultiwayTrie node.
     * Otherwise, the instance of the key is compared directly against the supplied Object.
     *
     * @param o The Object to performance an equality comparison against.
     * @return Whether the Objects are deemed to be equal.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof MultiwayTrie) {
            return this.key.equals(((MultiwayTrie) o).key);
        } else {
            return this.key.equals(o);
        }
    }
}
