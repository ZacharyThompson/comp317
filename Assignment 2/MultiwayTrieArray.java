/**
 * Multiway Trie implementation that uses an array as the underlying mechanism to
 * insert and search children. There are scenarios where this implementation is
 * highly beneficial, likewise there are scenarios where this implementation would
 * have horrendous performance implications.
 *
 * For an LZWEncoder, the first layer of the tree is populated with the symbol set.
 * Assuming the symbol set is known and a numeric key is used, using an array to index
 * the value would allow for O(1) insert and search operations.
 *
 * If, however, the tree may have a dynamic number of children wherein the final amount
 * a tree will have is unknown at any given time, using a fixed array would have significant
 * performance implications.
 *
 * @param <K> The type of keys maintained by this Trie.
 * @param <V> The type of elements in this Trie.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class MultiwayTrieArray<K extends Number, V> extends MultiwayTrie<K, V> {
    /**
     * The children of this Multiway Trie instance, held in an array.
     */
    private MultiwayTrie<K, V>[] children;

    /**
     * The offset applied to key values to correctly index in the children array.
     */
    private int offset;

    /**
     * Constructs a Multiway Trie instance with a key, value pair that are mapped to a
     * parent Trie instance. The children array is instantiate using the defined size
     * and the offset is saved to ensure correct indexing into the children values.
     *
     * @param key Key of which the specified value is associated with.
     * @param value Value to be associated with the key.
     * @param parent The parent MultiwayTrie node of this instance.
     * @param size The size of the children array.
     * @param offset The offset to apply to the key when indexing a child.
     */
    @SuppressWarnings("unchecked")
    public MultiwayTrieArray(K key, V value, MultiwayTrie<K, V> parent, int size, int offset) {
        super(key, value, parent);
        this.offset = offset;
        children = new MultiwayTrie[size];
    }

    /**
     * Constructs a Multiway Trie instance with a key, value pair. The children array
     * is instantiate using the defined size and the offset is saved to ensure correct
     * indexing into the children values.
     *
     * @param key Key of which the specified value is associated with.
     * @param value Value to be associated with the key.
     * @param size The size of the children array.
     * @param offset The offset to apply to the key when indexing a child.
     */
    public MultiwayTrieArray(K key, V value, int size, int offset) {
        this(key, value, null, size, offset);
    }

    /**
     * Inserts a key, value pair into the children array. The key is correctly mapped
     * to an index by taking the integer value of the key and applying the offset.
     *
     * @param key Key of which the specified value is associated with.
     * @param value Value to be associated with the key.
     * @throws IndexOutOfBoundsException If the index is out of bounds (index < 0 || index >= size)
     */
    @Override
    public void insert(K key, V value) {
        children[getIndex(key)] = new MultiwayTrieList<>(key, value);
    }

    /**
     * Searches for a value associated with a given key in the children array. The key
     * is correctly mapped to an index by taking the integer value of the key and applying
     * the offset. If no item was found within a valid index, null will be returned.
     *
     * @param key Key of which the target value is associated with.
     * @return The value associated with the given key.
     * @throws IndexOutOfBoundsException If the index is out of bounds (index < 0 || index >= size)
     */
    @Override
    public MultiwayTrie<K, V> search(K key) {
        return children[getIndex(key)];
    }

    /**
     * Converts a key to a valid index, checking if the index is valid before returning it.
     *
     * @param key The key to map the index to.
     * @return The index correctly mapped to the key.
     * @throws IndexOutOfBoundsException If the index is out of bounds (index < 0 || index >= size)
     */
    private int getIndex(K key) {
        int index = key.intValue() + offset;
        if (index >= children.length || index < 0)
            throw new IndexOutOfBoundsException("Index:" + index + "Size:" + children.length);
        return index;
    }
}
