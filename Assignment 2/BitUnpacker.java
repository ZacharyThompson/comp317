import java.io.*;

import static java.lang.System.lineSeparator;

/**
 * A bit unpacking implementation that is complemented by the BitPacker class. The implementation
 * only works for the LZW encoding algorithm implemented as part of Assignment 2 of COMP317-18A.
 * It leverages the fact that we know the maximum phrase number, hence can efficiently represent
 * every phrase number with the minimum number of bits such that there are no ambiguous values.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class BitUnpacker {
    /**
     * The byte cache that has yet to be consumed.
     */
    private int cache;

    /**
     * The current bit in the byte cache.
     */
    private int currentBit;

    /**
     * Constructs a BitUnpacker Object that effectively reads data from the given input stream
     * and attempts to convert the values to phrase numbers using the maximum number of
     * bits needed to fully disambiguate all integers.
     *
     * @param input The input stream for which the bit-packed data is read from.
     * @param output The output stream for which the phrase numbers can be written to.
     */
    public BitUnpacker(InputStream input, OutputStream output) {
        try (
                BufferedInputStream reader = new BufferedInputStream(input);
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output))
        ) {
            // Reads the first byte into input.
            cache = reader.read();
            int phrase;
            int phraseCount = 257;
            int nextCount = 512;
            int bits = 9;
            // Read in an integer using the specified number of bits until there is no longer any input remaining.
            while ((phrase = readBits(reader, bits)) != -1) {
                // Write the phrase number.
                writer.append(Integer.toString(phrase)).append(lineSeparator());
                writer.flush();
                // Check if the dictionary was reset.
                if (phrase == 0) {
                    // If the dictionary was reset, there are no 257 known phrases in the dictionary.
                    phraseCount = 257;
                    nextCount = 512;
                    bits = 9;
                } else {
                    // Otherwise, increment the number of phrases and update the number of bits required.
                    phraseCount++;
                    if (phraseCount > nextCount) {
                        bits++;
                        nextCount = nextCount << 1;
                    }
                }
            }
            writer.flush();
        } catch (IOException e) {
            System.err.println("An IO error occurred while attempting to unpack the supplied bits");
        }
    }

    /**
     * Reads data from a BufferedInputStream byte per byte, constructing integers that are encoded using
     * a specified number of bits.
     *
     * @param inputStream The InputStream to read the data from.
     * @param length The number of bits to use to construct the integer.
     * @return The next integer from the InputStream that was encoded over the next specified number of bits.
     * @throws IOException Thrown when an error occurred in reading data from the BufferedInputStream.
     */
    private int readBits(InputStream inputStream, int length) throws IOException {
        int value = cache;
        for (length += currentBit; length > 8; length -= 8) {
			currentBit = 0;
            if ((cache = inputStream.read()) == -1)
                return -1;
            value = (value << 8) | cache;
        }
        value = (value | cache) >> (8 - length);
        currentBit += length;
        cache &= 0xFF >> currentBit;
        return value;
    }

    public static void main (String args[]) {
        if (args.length > 0) {
            System.err.println("Invalid arguments: Expected 'java BitUnpacker");
            System.exit(0);
        }
        BitUnpacker packer = new BitUnpacker(System.in, System.out);
    }
}
