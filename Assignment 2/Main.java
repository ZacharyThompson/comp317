import java.io.*;

public class Main {
    public static void main (String args[]) throws Exception {
		if (args.length != 1) {
			System.err.println("Invalid Arguments : Expected java Main <source file>");
			System.exit(0);
		}
        File input = new File(args[0]);
		if (!input.exists()) {
			System.err.println("Invalid Arguments : Source file does not exist");
			System.exit(0);
		}
		
        File encoded = new File(input, "../encoded");
        File packed = new File(input, "../packed");
        File unpacked = new File(input, "../unpacked");
        File output = new File(input, "../output." + getFileExtension(input));

        LZWEncoder encoder = new LZWEncoder(new FileInputStream(input), new FileOutputStream(encoded), 16);
        //BitPacker packer = new BitPacker(new FileInputStream(encoded), new FileOutputStream(packed));
        //BitUnpacker unpacker = new BitUnpacker(new FileInputStream(packed), new FileOutputStream(unpacked));
		OutputStream os = new FileOutputStream(output);
        LZWDecoder decoder = new LZWDecoder(new FileInputStream(encoded), os);
		os.close();
	}
	
	private static final String getFileExtension(File file) {
		try {
			return file.getName().substring(file.getName().lastIndexOf(".") + 1);
		} catch (IndexOutOfBoundsException e) {
			return "";
		}
	}
}
