import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A Lempel–Ziv–Welch decoding implementation that decodes the output from the LZWEncoder class.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class LZWDecoder {
    /**
     * The current phrase count.
     */
    private int phraseCount;

    /**
     * The dictionary containing the known phrases as interpreted from the encoded input.
     */
    private List<Phrase> dictionary;

    /**
     * Constructs a decoder that will attempt to decode a given input stream and output
     * the decoded bytes to the given output stream. The input is assumed to be integers
     * separated by new line characters.
     *
     * @param input The source input stream to be decoded.
     * @param output The output stream for which the decoded input should be written to.
     */
    public LZWDecoder(InputStream input, OutputStream output) {
        resetDictionary();
        try (
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
				BufferedOutputStream bufferedOutput = new BufferedOutputStream(output)
        ) {
            Phrase nextPhrase, prevPhrase = new Phrase(null, null);
            String line;
            int index;
			boolean growing = true;
            // Read a line into input.
            while ((line = reader.readLine()) != null) {
                // Parse the phrase number.
                index = Integer.parseInt(line);
                // Check if the dictionary was reset and continue.
                if (index == 0) {
					growing = true;
                    resetDictionary();
                    continue;
                } else if (index == 1) {
					growing = false;
					continue;
				}
                // Find the next phrase and infer mismatch from the root character.
                nextPhrase = dictionary.get(index - 2);
				if (growing) {
					prevPhrase.data = nextPhrase.getRoot().data;
					// Add new phrase to the dictionary.
					prevPhrase = new Phrase(null, nextPhrase);
					dictionary.add(phraseCount++, prevPhrase);					
				}
				// Write the phrase.
				nextPhrase.write(bufferedOutput);
                bufferedOutput.flush();
            }
        } catch (IndexOutOfBoundsException e) {
			System.err.println("An invalid phrase number was encountered while attempting to decode.");
		}catch (NumberFormatException e) {
            System.err.println("A non numeric value was encountered while attempting to decode.");
        } catch (IOException e) {
            System.err.println("An IO exception occurred while attempting to decode.");
        }
    }

    /**
     * Resets the dictionary.
     */
    private void resetDictionary() {
        dictionary = new ArrayList<>();
        phraseCount = 0;
        for (int i = Byte.MIN_VALUE; i <= Byte.MAX_VALUE; i++) {
            dictionary.add(phraseCount++, new Phrase((byte) i, null));
        }
    }

    /**
     * Represents a phrase that has been inferred from the decoding process.
     * Each phrase is represented by a mismatch Byte and the previous phrase.
     *
     */
    private class Phrase {
        /**
         * The mismatch byte
         */
        private Byte data;

        /**
         * The parent Phrase that the mismatch byte should be appended to.
         */
        private Phrase parent;

        /**
         * Constructs a phrase from a byte and an existing phrase.
         *
         * @param data The mismatch byte.
         * @param parent The parent phrase.
         */
        public Phrase(Byte data, Phrase parent) {
            this.data = data;
            this.parent = parent;
        }

        /**
         * Gets the root phrase associated with this phrase by recursive invocation.
         *
         * @return The root phrase.
         */
        public Phrase getRoot() {
            return (parent == null) ? this : parent.getRoot();
        }

        /**
         * Gets the mismatch byte of the phrase.
         *
         * @return The mismatch.
         */
        public Byte getData() {
            return data;
        }

        /**
         * Writes the phrase to an OutputStream big endian by recursively finding the root
         * phrase and writing the Byte as the stack unwinds. Using an OutputStream has a
         * significant performance advantage over alternative Objects.
         *
         * @param outputStream The OutputStream to write the phrase to.
         * @throws IOException Thrown when an error occurred in writing to the OutputStream.
         */
        public void write(OutputStream outputStream) throws IOException {
            if (this.parent != null)
                this.parent.write(outputStream);
			if (this.data != null)
				outputStream.write(this.data);
        }
    }

    public static void main (String args[]) {
        if (args.length > 0) {
            System.err.println("Invalid arguments: Expected 'java LZWDecoder");
            System.exit(0);
        }
        LZWDecoder decoder = new LZWDecoder(System.in, System.out);
    }
}