import java.io.*;

/**
 * A bit packing implementation that is complemented by the BitUnpacker class. The implementation
 * only works for the LZW encoding algorithm implemented as part of Assignment 2 of COMP317-18A.
 * It leverages the fact that we know the maximum phrase number, hence can efficiently represent
 * every phrase number with the minimum number of bits such that there are no ambiguous values.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class BitPacker {
    /**
     * The byte cache that has yet to be consumed.
     */
    private int cache;

    /**
     * The current bit in the byte cache.
     */
    private int currentBit;

    /**
     * Constructs a BitPacker Object that reads integers from the given input stream
     * and attempts to compress by removing redundant bits as the maximum number of
     * bits needed to fully disambiguate all integers is known.
     *
     * @param input The input stream for which the phrase numbers can be read from.
     * @param output The output stream for which the bit-packed data is written to.
     */
    public BitPacker(InputStream input, OutputStream output) {
        try (
                BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                BufferedOutputStream writer = new BufferedOutputStream(output)
        ) {
            String line;
            int phrase = 0;
            int phraseCount = 257;
            int nextCount = 512;
            int bits = 9;
            // Read a line into input.
            while ((line = reader.readLine()) != null) {
                // Parse the phrase number.
                phrase = Integer.parseInt(line);
                // Write the phrase number using the minimum number of bits possible.
                writeBits(writer, phrase, bits);
                writer.flush();
                // Check if the dictionary was reset
                if (phrase == 0) {
                    // If the dictionary was reset, there are now 257 known phrases.
                    phraseCount = 257;
					nextCount = 512;
					bits = 9;
                } else {
                    // Otherwise, increment the number of phrases and update the number of bits required.
                    phraseCount++;
                    if (phraseCount >= nextCount) {
                        bits++;
                        nextCount = nextCount << 1;
                    }
                }
            }
            // Flush any excess bits
            writeBits(writer, 0, 7);
            writer.flush();
        } catch (NumberFormatException e) {
            System.err.println("A non numeric value was encountered while attempting to bit-pack.");
        } catch (IOException e) {
            System.err.println("An IO exception occurred while attempting to bit-pack.");
        }
    }

    /**
     * Writes an integer to the given BufferedOutputStream using the specified number of bits.
     *
     * @param outputStream The OutputStream wherein the data is to be written to.
     * @param value The integer to be written.
     * @param bits The number of bits used to encode the integer.
     * @throws IOException Thrown when an error occurred in writing to the BufferedOutputStream.
     */
    private void writeBits(OutputStream outputStream, int value, int bits) throws IOException {
        for (bits += currentBit; bits >= 8; bits -= 8) {
            outputStream.write(cache | (value >>> (bits - 8)));
            cache = 0;
            currentBit = 0;
        }
        cache |= (value << 8 - bits);
        currentBit += bits;
    }

    public static void main (String args[]) {
        if (args.length > 0) {
            System.err.println("Invalid arguments: Expected 'java BitPacker");
            System.exit(0);
        }
        BitPacker packer = new BitPacker(System.in, System.out);
    }
}
