import java.io.*;

/**
 *
 * A BufferedReaper 'Wrapper'. The class utilizes the BufferedReader class as the means of retrieving
 * data from a source, however, provides implementation to 'peek' at the next item in the stream.
 * Inheritance was omitted to avoid over consuming data and to resolve issues with comparing the Object
 * against a past instance of itself.
 *
 * The peeking element allows a BufferedReader to be sorted by the next element in the stream. This cannot be
 * achieved with a BufferedReader as reading a line consumes it from the source, meaning the continuous sorting
 * process would consume information that should not be lost.
 *
 * The class provides functionality to clone the Object as means of comparing against a past version of itself.
 * To utilize the replacement artificial limiting heap functionality, the data being inserted must be less important
 * than the data being extracted. The PeekableBufferedReader cannot ensure that the next element is less important
 * than the previous, hence consuming the line from the top of the heap is unable to retain the contract that the last
 * element out is always more important the the top of the heap. Furthermore, comparing the same instance against
 * itself will always result in an equivalence result, as this is guaranteed by the Object equivalence contract.
 * As such, the PeekableBufferedReader allows the user to clone it which results in a new PeekableBufferedReader instance
 * that shares the same BufferedReader. As the BufferedReader is shared, consuming lines on the clone or the original instance
 * will result in the other instance missing data which should be considered carefully. Cloning an instance will return
 * a new instance that has just consumed the next line; as such, the two instances are distinct in that the cloned is
 * one line ahead of the original.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class PeekableBufferedReader implements Comparable<PeekableBufferedReader>, Closeable, Cloneable {
    /**
     * The BufferedReader that will be peeked at.
     */
    private BufferedReader br;

    /**
     * The current line from the BufferedReader.
     */
    private String line;

    /**
     * Constructs a PeekableBufferedReader instance by instantiation of a BufferedReader
     * using a supplied Reader.
     *
     * @param reader The reader used to create the associated BufferedReader instance.
     */
    public PeekableBufferedReader(Reader reader) {
        this(new BufferedReader(reader));
    }

    /**
     * Constructs a PeekableBufferedReader instance from a supplied BufferedReader
     *
     * @param reader The buffered reader to be associated with this instance.
     */
    public PeekableBufferedReader(BufferedReader reader) {
        br = reader;
        this.peek();
    }

    /**
     * Peeks at the BufferedReader. This will not consume the line if a line has been read, however,
     * the function will attempt to read a line if there is no line currently held.
     *
     * @return The line currently held to be peeked at.
     */
    public String peek() {
        if (line == null) {
            try {
                line = br.readLine();
            } catch (Exception e) {
                line = null;
            }
        }
        return line;
    }

    /**
     * Consumes a line from the BufferedReader, updating the line that is currently held.
     *
     * @return The line consumed from the BufferedReader.
     */
    public String readLine() {
        String old = line;
        try {
            line = br.readLine();
        } catch (Exception e) {
            line = null;
        }
        return old == null ? line : old;
    }

    /**
     * Compares to another PeekableBufferedReader by comparing the lines currently
     * held in either.
     *
     * @param p The PeekableBufferedReader instance to compare the current to.
     * @return The comparison between the two Objects.
     */
    @Override
    public int compareTo(PeekableBufferedReader p) {
        if (p == null || p.peek() == null || peek() == null)
            throw new NullPointerException();

        return this.peek().compareTo(p.peek());
    }

    /**
     * Clones the current Object by instantiating a new PeekableBufferedReader using
     * the same BufferedReader.
     *
     * @return A clone of the current Object.
     */
    public PeekableBufferedReader clone() {
        return new PeekableBufferedReader(br);
    }

    /**
     * Closes the underlying BufferedReader.
     *
     * @throws IOException IOException thrown when attempting to close the BufferedReader.
     */
    @Override
    public void close() throws IOException {
        br.close();
    }

    /**
     * Instantiates an instance of PeekableBufferedReader from a File.
     *
     * @param file The file to create an instance of PeekableBufferedReader from.
     * @return The PeekableBufferedReader instance.
     */
    public static PeekableBufferedReader FromFile(File file) {
        try {
            return new PeekableBufferedReader(new FileReader(file));
        } catch (Exception e) {
            return null;
        }
    }
}
