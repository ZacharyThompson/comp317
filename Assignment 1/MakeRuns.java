import java.io.*;

/**
 *
 * The MakeRuns class that is used to create the initial runs for the external implementation.
 * The implementation makes use of a Heap implementation to sort an input file into a set of
 * runs that will be, on average, 2K, where K is the specified maximum heap size.
 *
 * The class provides a default entry point (main) that will create the initial runs for a specified
 * file using a Heap of a specified size.
 * Expected Usage: java MakeRuns <heap size> <file path>
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class MakeRuns {
    /**
     * The total numbers of runs produced.
     */
    private static int runs;

    /**
     * A default entry point implementation to demonstrate the functionality of the initial run
     * making process.
     *
     * @param args The command line arguments, expected: java MakeRuns <heap size> <file path>
     */
    public static void main(String[] args) {
        int heapSize = 0;
        File input;

        // Check the argument length
        if (args.length != 2) {
            System.err.println("Invalid arguments: Expected 'java MakeRuns <heap size> <file path>");
            System.exit(0);
        }

        // Check if the heap size argument is valid.
        try {
            heapSize = Integer.parseInt(args[0]);
            if (heapSize <= 1) {
                System.err.println("Invalid arguments: Heap size is too small");
                System.exit(0);
            }
        } catch (NumberFormatException e) {
            System.err.println(String.format("Invalid arguments: %s is not an integer", args[0]));
            System.exit(0);
        }

        // Check if the file path argument is valid.
        input = new File(args[1]);
        if (!input.exists()) {
            System.err.println(String.format("Invalid arguments: %s does not exist", args[1]));
            System.exit(0);
        }

        // Begin the make run process.
        make(input, heapSize);

        // Print the number of runs produced.
        System.err.println(String.format("Initial Runs Produced: %d", runs));
    }

    /**
     * Reads data from a given input file and produces a file containing the initial runs.
     *
     * @param input The input file of data to be sorted into initial runs.
     * @param heapSize The size of the heap to use.
     * @return The file containing the initial runs.
     */
    public static File make(File input, int heapSize) {
        // Check the arguments.
        if (input == null || heapSize <= 1)
            throw new IllegalArgumentException();

        // Create the output file.
        File output = new File(input.getAbsoluteFile() + ".runs");
        Heap<String> heap = new Heap<>(heapSize);

        // Use the try-resource with a BufferedReader and BufferedWriter.
        try (BufferedReader reader = new BufferedReader(new FileReader(input));
             BufferedWriter writer = new BufferedWriter(new FileWriter(output))
        ) {
            String datum;
            // If there is available data or there still remains data in the heap
            // continue sorting data.
            while ((datum = reader.readLine()) != null || heap.getTrueSize() > 0) {
                // If the datum is null, there is nothing to insert. So only extract
                // the root to append to the output stream. If the datum isn't null,
                // replace the root with the new datum before appending the root.
                if (datum == null) {
                    writer.append(heap.extract()).append(System.lineSeparator());
                } else if (!heap.offer(datum)) {
                    writer.append(heap.replace(datum)).append(System.lineSeparator());
                }
                // If the artificial size is 0, remove artificial limit and restore heap.
                // It can be inferred that a run has been completed.
                if (heap.getArtificialSize() == 0) {
                    heap.heapify();
                    runs++;
                }
            }
        } catch (IOException e) {
            System.err.println("An error occurred making the initial runs");
        }
        return output;
    }
}
