import java.io.*;
import java.util.ArrayList;

/**
 *
 * The MergeRuns class that is used to merge the initial runs created in the MakeRuns class, as the second
 * component of an external implementation. Like MakeRuns, the implementation makes use of a Heap implementation
 * to sort merge K runs into a single run, where K is the maximum number of runs to merge at a given time. More
 * specifically, K defines the maximum heap size.
 *
 * The class provides a default entry point (main) that will merge the runs contained in a specified
 * file using a Heap of a specified size.
 * Expected Usage: java MergeRuns <K> <file path>
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class MergeRuns {
    /**
     * The number of passes made.
     */
    private static int passes = 0;

    /**
     * The temporary file counter.
     */
    private static int temp = 0;

    /**
     * A default entry point implementation to demonstrate the functionality of the merging runs
     * functionality.
     *
     * @param args The command line arguments, expected: java MergeRuns <K> <file path>
     */
    public static void main(String args[]) {
        int k = 0;
        File input;

        // Check the argument length
        if (args.length != 2) {
            System.err.println("Invalid arguments: Expected 'java MergeRuns <K> <file path>");
            System.exit(0);
        }

        // Check if the k argument is valid.
        try {
            k = Integer.parseInt(args[0]);
            if (k <= 1) {
                System.err.println("Invalid arguments: K is too small");
                System.exit(0);
            }
        } catch (NumberFormatException e) {
            System.err.println(String.format("Invalid arguments: %s is not an integer", args[0]));
            System.exit(0);
        }

        // Check if the file path argument is valid.
        input = new File(args[1]);
        if (!input.exists()) {
            System.err.println(String.format("Invalid arguments: %s does not exist", args[1]));
            System.exit(0);
        }

        // Distribute and merge runs.
        ArrayList<File> files = distribute(input, k);
        File sorted = merge(files, k);

        // Rename the final file to include the suffix "sorted"
        String fileName = input.getAbsolutePath().substring(0, input.getAbsolutePath().lastIndexOf("."));
        File output = new File(fileName + ".sorted");
        if (output.exists())
            output.delete();
        sorted.renameTo(output);

        // Print the number of passes required.
        System.err.println(String.format("Number Of Passes Required: %d", passes));
    }


    /**
     * Merges the runs from a list of files using a maximum specified heap size.
     * The function uses a recursive approach to merge the runs within a set of
     * files until there is only one file remaining.
     *
     * @param inputs The input files containing the runs to be merged.
     * @param k The parameter for the k-way merge. Defines the maximum heap size.
     * @return The final file containing the fully sorted data.
     */
    public static File merge(ArrayList<File> inputs, int k) {
        // Check the arguments
        if (inputs == null || inputs.size() == 0 || k <= 1)
            throw new IllegalArgumentException();

        // The base case for the recursive function.
        if (inputs.size() == 1)
            return inputs.get(0);

        // Increment pass
        passes++;

        // Create the initial collection Objects.
        ArrayList<File> files = new ArrayList<>();
        BufferedWriter[] writers = new BufferedWriter[k];
        // Creates an array of PeekableBufferedReaders from a list of files
        PeekableBufferedReader[] readers = inputs.stream()
                .map(PeekableBufferedReader::FromFile)
                .filter(p -> p != null && p.peek() != null)
                .toArray(PeekableBufferedReader[]::new);
        // Create the initial heap from the BufferedReaders. Note that the maximum
        // size will be defined, but the heap will not be in heap order.
        Heap<PeekableBufferedReader> heap = new Heap<>(readers);

        try {
            int index = -1;
            while (heap.getTrueSize() > 0) {
                // If the artificial size of the heap is 0, remove artificial limit
                // and restore the heap order. This indicates that a run was completed,
                // so move to the next output file.
                if (heap.getArtificialSize() == 0) {
                    index = (index + 1) % k;
                    if (files.size() <= index) {
                        files.add(newTempFile(inputs.get(0)));
                        writers[index] = new BufferedWriter(new FileWriter(files.get(index)));
                    }
                    heap.heapify();
                }
                // Print out the root item before cloning it to consume a line.
                writers[index].append(heap.peek().peek()).append(System.lineSeparator());
                PeekableBufferedReader clone = heap.peek().clone();
                // If the cloned PeekableBufferedReader is finished, extract the root. Otherwise
                // replace the root with the cloned item.
                if (clone.peek() == null) {
                    heap.extract().close();
                } else {
                    heap.replace(clone);
                }
            }
            // Close the writers and readers
            closeArray(writers);
            closeArray(readers);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Delete temporary files
        for (File file : inputs) {
            file.delete();
        }
        return merge(files, k);
    }

    /**
     * Distributes the initial runs produced by MakeRuns into a collection of Files
     * evenly. It will produce, at most, K many files where K is the parameter for
     * the K-way merge.
     *
     * @param input The input file containing the initial runs produced by MakeRuns.
     * @param k The parameter for the k-way merge. Defines the maximum heap size.
     * @return A collection of files which contain the initial runs, evenly distributed.
     */
    public static ArrayList<File> distribute(File input, int k) {
        // Check the arguments
        if (input == null || k <= 1)
            throw new IllegalArgumentException();

        // Create the initial Collection Objects.
        ArrayList<File> files = new ArrayList<>();
        BufferedWriter[] writers = new BufferedWriter[k];

        // Open a buffered reader to the target file containing the initial runs.
        try (BufferedReader reader = new BufferedReader(new FileReader(input))) {
            String datum, prev = null;
            int index = -1;
            while ((datum = reader.readLine()) != null) {
                // Check if a run has been completed and switch output if so.
                if (prev == null || prev.compareTo(datum) > 0) {
                    index = (index + 1) % k;
                    if (files.size() <= index) {
                        files.add(newTempFile(input));
                        writers[index] = new BufferedWriter(new FileWriter(files.get(index)));
                    }
                }
                // Print the datum and save as previous
                writers[index].append(datum).append(System.lineSeparator());
                prev = datum;
            }
            // Close the writers
            closeArray(writers);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return files;
    }

    /**
     * Closes an array of Closeable Objects by simple iteration.
     *
     * @param closeables The array of Closeable Objects to be closed.
     */
    private static void closeArray(Closeable[] closeables) {
        // Iterate over each Closeable Object and attempt to close it
        for (Closeable closeable : closeables) {
            if (closeable == null) continue;
            try {
                closeable.close();
            } catch (IOException e) {
                System.err.println("Unable to close Object");
            }
        }
    }

    /**
     * Simple helper method to produce a new Temporary file, using the pass number
     * to ensure a uniqueness.
     *
     * @param base The base name of the file to mimic.
     * @return The temporary file created.
     */
    private static File newTempFile(File base) {
        // Get the base file name and return a temporary file.
        String fileName = base.getAbsolutePath().substring(0, base.getAbsolutePath().lastIndexOf("."));
        return new File(String.format("%s.temp%d", fileName, temp++));
    }
}
