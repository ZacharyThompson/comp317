The submission includes four source files.
1. MakeRuns: The first part of the assignment that takes an input file and integer as the maximum heap size and creates the initial runs.
2. MergeRuns: The second part of the assignment that takes an input file and integer as the K parameter to perform a K-way merge on the file.
3. Heap: A heap implementation that provides artifical restriction to ensure a datum being replaced with the root is less important.
4. PeekableBufferedReader: Allows to peek at the next line from a BufferedReader and can be used as datum within the Heap implementation.

Assumptions:
1. Temporary files used in the merge process should be deleted.
2. Error checking should be performed to avoid undefined, or not well-defined, behaviour from being encountered.
3. While certain restrictions have been put on the input, it is assumed that exhaustion of system resources will be handled by internally (OutOfMemoryError, etc). 

Notes:

End of Run markers are not used, rather the end of run is distinguished by the next element being smaller then the previous, which in turn has a slight 
impact on performance. While using End of Run markers would intuitively have a performance advantage (As the runs are in order, the String.compareTo(Object o)
function would be expected to have near worst case performance), not using End of Run markers is more robust. 
Regardless, ideally the MakeRuns and DistributeRuns processes would not be segregated, as to avoid the neccesity of finding the end of runs.

The three key components of the external merge process (create runs, distribute runs and merge runs) shared common implementation.
The implementation could have, and was, been implemented such that all three values shared a common function, While this wouldn't improve performance
for the assignment, it avoids redundancies that are present currently which exist due to limitations inflicted in the assignment specifications.
