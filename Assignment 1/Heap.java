import java.util.Arrays;
import java.util.Comparator;
import java.util.NoSuchElementException;

/**
 *
 * A Heap implementation that is bounded by the parameters defined at instantiation.
 * The implementation provides a replace function that takes an item to add to the heap
 * and will extract and return the root of the heap.
 * <p>
 * The exchange is done as follows:
 * If the given item is greater than or equal to the root item, a straight swap occurs.
 * <br>
 * If the given item is less than the root item, however, the size of the Heap is artificially
 * restricted and the item to added is indexed in memory outside the artificial restriction of
 * the heap. The root is swapped with the last item in the artificially restricted
 * <br>
 * Following the replacement, the heap order of the artificially restricted heap is restored.
 *
 * @param <T> The type of Objects held in the Heap implementation.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class Heap<T extends Comparable<T>> {
    /**
     * The generic Array that houses the elements of the heap in memory.
     */
    private T[] data;

    /**
     * The true size of the Heap, recognising how many elements in total
     * are currently stored in the Heap regardless of heap order.
     */
    private int trueSize;

    /**
     * The artificial size of the Heap. The heap is ensure to be within heap
     * order within the artificial size of the heap.
     */
    private int artificialSize;

    /**
     * The comparator used to compare elements within the heap.
     */
    private Comparator<T> comparator;

    /**
     * Constructs a heap of a defined size that sorts using a specified
     * comparator.
     *
     * @param size The maximum size allocated to the heap.
     * @param comparator The comparator used to define heap ordering.
     */
    @SuppressWarnings("unchecked")
    public Heap(int size, Comparator<T> comparator) {
        data = (T[]) new Comparable[size];
        this.comparator = comparator;
    }

    /**
     * Constructs a heap from a given array of unsorted Objects. The size
     * of the given array is used as the maximum size of the heap. The array
     * is converted into a heap following the defined ordering of the given
     * comparator.
     *
     * @param data An array of unsorted to convert into a heap.
     * @param comparator The comparator used to define heap ordering.
     */
    public Heap(T[] data, Comparator<T> comparator) {
        this.data = data;
        this.comparator = comparator;
        while (data.length > trueSize && data[trueSize] != null) {
            trueSize++;
        }
    }

    /**
     * Constructs a heap of a defined size using natural ordering.
     *
     * @param size The maximum size allocated to the heap.
     */
    public Heap(int size) {
        this(size, Comparator.naturalOrder());
    }

    /**
     * Constructs a heap from a given array of unsorted Objects. The size
     * of the given array is used as the maximum size of the heap. The array
     * is converted into a using natural ordering.
     *
     * @param data An array of unsorted to convert into a heap.
     */
    public Heap(T[] data) {
        this(data, Comparator.naturalOrder());
    }

    /**
     * Peaks at the node of the heap without changing the order.
     *
     * @return The root Object of the heap implementation.
     */
    public T peek() {
        if (artificialSize == 0)
            throw new NoSuchElementException();
        return data[0];
    }

    /**
     * Inserts an item into the heap at a tail node and will restore the ordering
     * using the upheap operation.
     *
     * @param datum The item to add to the heap.
     */
    public void insert(T datum) {
        if (datum == null)
            throw new NullPointerException();
        if (trueSize > data.length)
            throw new OutOfMemoryError();

        data[trueSize] = data[artificialSize];
        data[artificialSize++] = datum;
        upHeap(trueSize++);
    }

    /**
     * Extracts the root Object from the heap implementation.
     *
     * @return The item that has been extracted from the heap.
     */
    public T extract() {
        if (artificialSize == 0)
            throw new NoSuchElementException();
        T extracted = data[0];
        data[0] = data[--artificialSize];
        data[artificialSize] = data[--trueSize];
        data[trueSize] = null;
        downHeap(0);
        return extracted;
    }

    /**
     * Recursive up heap implementation that is used to restore
     * heap order. Starts at a given index in the heap and works
     * it's way up the heap until it has inferred heap order has
     * been restored.
     *
     * @param index The index to start the up heap operation from.
     */
    private void upHeap(int index) {
        if (index > 0) {
            int parent = getParentIndex(index);
            if (comparator.compare(data[index], data[parent]) < 0) {
                swap(data, index, parent);
                upHeap(parent);
            }
        }
    }

    /**
     * Recursive down heap implementation that is used to restore
     * heap order. Starts at a given index in the heap and works
     * it's way down the heap until it has inferred heap order has
     * been restored.
     *
     * @param index The index to start the down heap operation from.
     */
    private void downHeap(int index) {
        int child = getLeftIndex(index);
        if (child < artificialSize) {
            child = (child + 1 >= artificialSize
                        || comparator.compare(data[child], data[child + 1]) < 0)
                    ? child : child + 1;

            if (comparator.compare(data[index], data[child]) > 0) {
                swap(data, index, child);
                downHeap(child);
            }
        }
    }

    /**
     * Restores the heap to the true size while restoring heap
     * order as it expands. This method, in it's current standing,
     * may not be optimal.
     *
     */
    public void heapify() {
        while (artificialSize < trueSize) {
            upHeap(artificialSize++);
        }
    }

    /**
     * Offers an item to add to the heap using the insertion method.
     *
     * @param datum The item to add to the heap.
     * @return Whether the item was successfully added.
     */
    public boolean offer(T datum) {
        if (trueSize >= data.length || datum == null)
            return false;
        insert(datum);
        return true;
    }

    /**
     * Recursive method to find whether the binary tree, artificially restricted,
     * is currently a heap. With this implementation, the only possible
     * reason the heap may not be valid is if it is not in heap order. This method
     * would be used to ensure that at any given time the provided heap is in
     * heap order, which is useful for debugging purposes.
     *
     * @param index The index to start the validity check from.
     * @return Whether the tree, starting at the index, is a valid heap.
     */
    private boolean isValidHeap(int index) {
        if (index >= artificialSize)
            return true;
        int child = getLeftIndex(index);
        if (child + 1 < artificialSize && comparator.compare(data[index], data[child + 1]) > 0
                || child < artificialSize && comparator.compare(data[index], data[child]) > 0) {
            return false;
        }
        return isValidHeap(child) && isValidHeap(child + 1);
    }

    /**
     * Extracts the item at the root while simultaneously inserting a new item.
     * If the new item to be inserted is less than the root, the last item in the
     * heap is moved to the root and the new item is added to the end of the heap.
     * The size of the heap is then artificially restricted by one. If, however, the
     * new item is greater than or equal to the root, it directly replaces the root.
     * The down heap operation is used to restore heap order.
     *
     * @param datum The item to be added.
     * @return The item removed from the heap.
     */
    public T replace(T datum) {
        T old = data[0];
        if (old != null && comparator.compare(datum, old) < 0) {
            if (artificialSize == 0)
                throw new OutOfMemoryError();

            data[0] = data[--artificialSize];
            data[artificialSize] = datum;
        } else {
            data[0] = datum;
        }
        downHeap(0);
        return old;
    }

    /**
     * Swaps items in an array given two indexes.
     *
     * @param data The array to swap values from.
     * @param a The first index.
     * @param b The second index.
     * @param <T> The type of data to swap.
     */
    private static <T> void swap(T[] data, int a, int b) {
        T old = data[a];
        data[a] = data[b];
        data[b] = old;
    }

    /**
     * The artificial size is used to artificially restrict the size of
     * the heap; items added outside of this size are not guaranteed to be
     * in heap order.
     *
     * @return The artificial size of the heap.
     */
    public int getArtificialSize() {
        return artificialSize;
    }

    /**
     * The true size is used to record how many elements are currently held
     * within the heap; This value may not exceed the true limit of the heap.
     *
     * @return The true size of the heap.
     */
    public int getTrueSize() {
        return trueSize;
    }

    /**
     * Calculates the index of the parent of the node.
     *
     * @param index The index of the element.
     * @return The index of the parent element.
     */
    public static int getParentIndex(int index) {
        return ((index + 1) >> 1) - 1;
    }

    /**
     * Calculates the index of the left child of the node.
     *
     * @param index The index of the element.
     * @return The index of the left child element.
     */
    public static int getLeftIndex(int index) {
        return ((index + 1) << 1) - 1;
    }

    /**
     * Calculates the index of the right child of the node.
     *
     * @param index The index of the element.
     * @return The index of the right child element.
     */
    public static int getRightIndex(int index) {
        return ((index + 1) << 1);
    }

    /**
     * Prints the heap using a standard recursive tree printing
     * format. Used primarily for debugging purposes. Should
     * be removed before submission.
     *
     * @param index The index to begin printing at. Recursively used.
     * @param spacing The spacing for the current node.
     */
    private void printTree(int index, int spacing) {
        if (index < trueSize) {
            printTree(getLeftIndex(index), spacing + 1);
            String spaces = new String(new char[spacing]).replace('\0', ' ');
            System.out.println(spaces + "|" + data[index]);
            printTree(getRightIndex(index), spacing + 1);
        }
    }

    /**
     * Basic print function to print the heap to the standard
     * output stream.
     */
    private void print() {
        System.out.println(Arrays.toString(data));
    }
}
