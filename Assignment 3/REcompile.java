package assignment3;

import javax.annotation.processing.SupportedSourceVersion;
import java.io.PrintStream;

import static assignment3.FiniteStateMachine.BRANCH;
import static assignment3.FiniteStateMachine.NEGATE;
import static assignment3.FiniteStateMachine.WILDCARD;

/**
 * A regex compiler that takes a defined regular expression pattern and attempts to build
 * a Finite State Machine that can be used to check whether the regular expression pattern
 * matches a given String.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class REcompile {
    /**
     * String corresponding to the set of symbols that form the regular expression.
     */
    private static final String SYMBOLS = "+*()[]!?|";

    /**
     * String corresponding to the set of reserved characters.
     */
    private static final String RESERVED = "" + BRANCH + NEGATE + WILDCARD + '\0';

    /**
     * The regular expression of which a Finite State Machine is to be built from.
     */
    private String re;

    /**
     * The current character of the regular expression.
     */
    private char c;

    /**
     * The current index in the String for which the current character refers to.
     */
    private int j = -1;

    /**
     * The FiniteStateMachine that is built from the regular expression.
     */
    private FiniteStateMachine fsm;

    /**
     * Constructs a REcompile Object that takes a given regular expression pattern
     * and attempts to compile it into a corresponding FiniteStateMachine that accepts
     * all input that matches the regular expression.
     *
     * @param regex The regular expression to build a FiniteStateMachine from.
     */
    public REcompile(String regex) {
        this.re = regex + '\0';
        compile();
    }

    /**
     * Gets the Finite State Machine that is built by this regular expression compiler.
     * If an error was encountered during compilation, the Finite State Machine will
     * be null.
     *
     * @return The Finite State Machine built.
     */
    public FiniteStateMachine getFiniteStateMachine() {
        return fsm;
    }

    /**
     * Prints the compiled regular expression to a PrintStream.
     *
     * @param printStream The PrintStream to write the compiled regular expression to.
     */
    public void print(PrintStream printStream) {
        fsm.print(printStream);
    }

    /**
     * Fetches the next character of the regular expression String that is being
     * compiled into a Finite State Machine.
     *
     */
    private void next() {
        if (++j >= re.length())
            error();
        c = re.charAt(j);
    }

    /**
     * Compiles the regular expression to a Finite State Machine.
     *  State 0 : The start state.
     *  State state : The final state.
     *
     */
    private void compile() {
        // Instantiate the FiniteStateMachine and fetch the first character.
        this.fsm = new FiniteStateMachine(re.length());
        next();
        // Branch from the first state.
        fsm.setState(0, BRANCH, 1, 1);
        fsm.state++;
        // Find the overarching expression.
        expression();
        // Check the end of the input was reached.
        if (re.charAt(j) != '\0')
            error();
    }

    /**
     * Finds the next expression.
     *
     * @return The index of the start of the expression.
     */
    private int expression() {
        // Find the next term and remember it's start character.
        int r = term();
        // If the next character is in the vocab or is an enclosing character.
        if (isVocab(c) || c == '(' || c == '[' || c == '!')
            expression();
        return r;
    }


    /**
     * Finds the next term.
     * The method takes a factor and checks to see if it is followed by an operator (*,+,?,|) and
     * will, in essence, encapsulate around the state machine for the created factor to apply the
     * operator functionality.
     *
     * @assumption Assumes that a (*,+,?) operator may be directly followed by another (*,+,?). While many
     * occasions of this form redundant patterns (v** = (v*)* = v*), there is one instance which is *technically*
     * not redundant (a+? = (aa*)? = λ|(aa*) = a*).
     *
     * @return The index of the start of the term.
     */
    private int term() {
        int f = fsm.state - 1;
        int r = factor();
        int t1 = r;
        int t2;

        // ToDO
        // Test functionality
        // Dangling meta characters
        while ("*+?".indexOf(c) >= 0) {
            if (c == '*') {
                // Match preceding regexp zero or more times.
                setStateBranch(f, r);
                fsm.setState(fsm.state, fsm.ch(r), fsm.next1(r), fsm.next2(r));
                fsm.setState(r, BRANCH, fsm.state, fsm.state + 1);
                next();
                r = fsm.state++;
            } else if (c == '+') {
                // Match preceding regexp one or more times.
                fsm.setState(fsm.state, BRANCH, t1, fsm.state + 1);
                next();
                r = fsm.state++;
            } else if (c == '?') {
                // Match preceding regexp zero or one time.
                setStateBranch(f, fsm.state + 1);
                fsm.setState(fsm.state, fsm.ch(r), fsm.next1(r), fsm.next2(r));
                fsm.setState(r, BRANCH, fsm.state, fsm.state + 1);
                next();
                r = fsm.state++;
            }
        }
        if (c == '|') {
            // Infix alternation that takes the form r|e, and matches r OR e.
            setStateBranch(f, fsm.state);
            f = fsm.state - 1;
            r = fsm.state++;
            next();
            t2 = fsm.state;
            expression();
            fsm.setState(r, BRANCH, t2, t1);
            setStateBranch(f, fsm.state);
        }
        return r;
    }

    /**
     * Finds the next factor.
     * A factor may take the form of:
     *   1. An element of the vocab
     *   2. An escaped token to be represented as a literal
     *   3. An expression enclosed in parenthesis (To raise the binding precedence)
     *   4. A set of literals enclosed in [E] (To denote match one of the literals)
     *   5. A set of literals enclosed in ![E]! (To denote match neither of the literals)
     *
     * @return The index of the start of the factor.
     */
    private int factor() {
        int r = fsm.state;
        // If the character is in the vocabulary
        if (isVocab(c)) {
            // Assign the wildcard character the reserved character.
			if (c == '.')
			    c = WILDCARD;
			// If the character was an escape token, consume the character.
            if (c == '\\')
                next();
            // Ensure the next character is not a reserved character.
            if (RESERVED.indexOf(c) != -1)
                error();
            // Create a state for the character and consume it.
            fsm.setState(fsm.state++, c, fsm.state, fsm.state);
            next();
        } else {
            if (c == '!') {
                // Matches one and only one literal NOT in the enclosed literals.
                next();
                negateAlternation();
            } else if (c == '[') {
                // Matches one and only one literal in the enclosed literals.
                next();
                alternation();
            } else if (c == '(') {
                // Find an expression to enclose in the parenthesis.
                next();
                r = expression();
                if (c != ')')
                    error();
                next();
            } else {
                error();
            }
        }
        return r;
    }

    /**
     * Defines a FiniteStateMachine that corresponds to an alternation
     * list of literals wherein the FiniteStateMachine must mach none.
     *
     * Implementation comes with two caveats:
     *    1. Don't consume a character when passing through
     *    2. A match occurs when the character does not match.
     *
     * In essence, this applies de Morgan's law
     *  ¬(A ∧ B) = (¬A ∨ ¬B)
     *
     * For the REGEX "![abc]!", the following FiniteStateMachine is created:
     *
     *  STATE   CHAR    NEXT1  NEXT2
     *  0       BRANCH  1      1
     *  1       NEGATE  2      2
     *  2       a       3      3
     *  3       b       4      4
     *  4       c       5      5
     *  5       NEGATE  6      6
     *
     * For the defined Finite State Machine, the state flow is as followed
     * for various input:
     *
     * search("c")
     *  (0) -> (1) -> (2) -> (3) -> (4) (HALT)
     * search("d")
     *  (0) -> (1) -> (2) -> (3) -> (4) -> (5) -> (6) (COMPLETE)
     * search("b")
     *  (0) -> (1) -> (2) -> (3) (HALT)
     *
     */
    private void negateAlternation() {
        // The next character must be the open bracket ('[')
        if (c != '[')
            error();
        // Add a NEGATE state
        fsm.setState(fsm.state++, NEGATE, fsm.state, fsm.state);
        next();
        // Blindly add the next character.
        fsm.setState(fsm.state++, c, fsm.state, fsm.state);
        next();
        // Iterate over the list of literals until the closed bracket
        // is encountered (']')
        while (c != ']') {
            // Define a logical AND state flow and consume the character.
            fsm.setState(fsm.state++, c, fsm.state, fsm.state);
            next();
        }
        // Add a NEGATE state
        fsm.setState(fsm.state++, NEGATE, fsm.state, fsm.state);
        next();
        // Ensure that the negate alternation list of literals is correctly closed.
        if (c != '!')
            error();
        next();
    }

    /**
     * Defines a FiniteStateMachine that corresponds to an alternation list
     * of literals.
     *
     * @assumption An assumption was made to neglect checking for repeating characters.
     * While this would increase the size of the FiniteStateMachine corresponding to
     * the alternation, it reduces the overhead of checking for duplicated characters.
     *
     */
    private void alternation() {
        // Create a branching state to two new states.
        fsm.setState(fsm.state++, BRANCH, fsm.state, fsm.state + 1);
        int s1 = fsm.state++;
        char c1 = c;
        // Consume a character. This will handle the case of the ] character appearing first.
        next();
        if (c == ']') {
			// If the end bracket had been found, unwind the stack to allow
            // the create states to point to the new final state.
            fsm.setState(s1 - 1, c1, --fsm.state, fsm.state);
            next();
        } else {
            // If the parenthesis was not encountered recursively repeat and
            // create an OR branching state for the character.
            alternation();
            fsm.setState(s1, c1, fsm.state, fsm.state);
        }
    }

    /**
     * Checks whether a given character is part of the vocabulary or, more specifically,
     * is not a symbol or a reserved character. Note that this will allow the escape character
     * through and assumes if there is no following token an error will be found when the next
     * character is requested.
     *
     * @param c The character to match
     * @return Whether the character is in the vocabulary.
     */
    private boolean isVocab(char c) {
        return SYMBOLS.indexOf(c) == -1 && RESERVED.indexOf(c) == -1;
    }

    /**
     * Sets the next states of a given state of the FiniteStateMachine.
     * If the two states of the given state match the method will change
     * both otherwise it will only change the second.
     *
     * @param state The state to change.
     * @param next The state to point to.
     */
    private void setStateBranch(int state, int next) {
        if (fsm.next1(state) == fsm.next2(state))
            fsm.next1(state, next);
        fsm.next2(state, next);
    }

    /**
     * Throws an error indicating an exception when attempting to parse the regular
     * expression.
     */
    private void error() {
        this.fsm = null;
        throw new RegexParseException(re, j);
    }

    /**
     * The RuntimeException to be executed to indicate that an error occurred when
     * attempting to parse/compile the regular expression
     */
    private static class RegexParseException extends RuntimeException {
        private RegexParseException(String regex, int index) {
            super(String.format("Exception on parsing String %s [index : %d]", regex, index));
        }
    }

    /**
     * Main entry point into the compiler.
     * Takes as input a regular expression String enclosed in quotation marks and prints
     * as standard output a state description of the Finite State Machine.
     *
     * Note that the REcompile will attempt to compile but an exception will be thrown
     * if the regular expression provided is not well formed.
     *
     * @param args The program arguments
     */
    public static void main (String args[]) {
        if (args.length == 0) {
            System.err.println("Invalid arguments: Expected 'java REcompile <Regular Expression>'");
            System.exit(0);
        }
        try {
            REcompile main = new REcompile(args[0]);
            main.print(System.out);
        } catch (RegexParseException ex) {
            System.err.println("Invalid regular expression: " + args[0]);
        }
    }
}
