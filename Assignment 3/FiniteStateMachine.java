package assignment3;

import java.io.PrintStream;

/**
 * A FiniteStateMachine implementation that is used to
 * remember the states of a FSM and their purpose.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class FiniteStateMachine {
    /**
     * The reserved characters for the FiniteStateMachine.
     */
    static final char BRANCH = '\1';
    static final char NEGATE = '\2';
	static final char WILDCARD = '\3';

    /**
     * The factor of increase when resizing the arrays.
     */
	private static final int INCREASE_FACTOR = 2;

    /**
     * The arrays that contain the FiniteStateMachine information.
     */
    private char[] ch;
    private int[] next1;
    private int[] next2;

    /**
     * The final state of the machine.
     */
    public int state;

    /**
     * Whether the FiniteStateMachine is consuming a character (And negating comparisons).
     */
    public boolean consume = true;

    /**
     * Instantiates a FiniteStateMachine with an initial size that
     * is an estimate of how large the FiniteStateMachine should be.
     * On average, the FiniteStateMachine should grow 0-1 times and
     * the input is the size of the regular expression String.
     *
     * @param initialSize The initial size of the FiniteStateMachine.
     */
    public FiniteStateMachine(int initialSize) {
        ch = new char[initialSize];
        next1 = new int[initialSize];
        next2 = new int[initialSize];
    }

    /**
     * Sets the character to match and the next states of a given state.
     *
     * @param s The state to set.
     * @param c The character to match
     * @param n1 The next state (1)
     * @param n2 The next state (2)
     */
    public void setState(int s, char c, int n1, int n2) {
       // Does the Machine need to increase it's size?
        if (s >= ch.length)
            increaseSize();
        // Assign the values
        ch[s] = c; next1[s] = n1; next2[s] = n2;
    }

    /**
     * Sets the next state (1) of a given state.
     *
     * @param state The state to set.
     * @param next The next state (1).
     */
    public void next1(int state, int next) {
        next1[state] = next;
    }

    /**
     * Sets the next state (2) of a given state.
     *
     * @param state The state to set.
     * @param next The next state (2).
     */
    public void next2(int state, int next) {
        next2[state] = next;
    }

    /**
     * Gets the next state (1) of a given state.
     *
     * @param state The state to get the next state (1) from.
     * @return The next state (1).
     */
    public int next1(int state) {
        return next1[state];
    }

    /**
     * Gets the next state (2) of a given state.
     *
     * @param state The state to get the next state (2) from.
     * @return The next state (2).
     */
    public int next2(int state) {
        return next2[state];
    }

    /**
     * Gets the character to match of a given state.
     *
     * @param state The state to get the char from.
     * @return The character to match.
     */
    public char ch(int state) {
        return ch[state];
    }

    /**
     * Prints the FiniteStateMachine to a PrintStream as a series of lines that
     * describe the various sates. The lines are comprised of four components,
     * separated by spaces:
     *  1. The state number
     *  2. The character to match
     *  3. The next state (1)
     *  4. The next state (2)
     *
     * @param printStream The PrintStream to write the output to.
     */
    public void print(PrintStream printStream) {
        for (int i = 0; i < state; i++) {
            printStream.println(String.format("%d %c %d %d", i, ch[i], next1[i], next2[i]));
        }
    }

    /**
     * Increases the size of the various arrays that form the FiniteStateMachine.
     */
    private void increaseSize() {
        char[] chCopy = new char[ch.length * INCREASE_FACTOR];
        System.arraycopy(ch, 0, chCopy, 0, ch.length);
        ch = chCopy;

        int[] next1Copy = new int[next1.length * INCREASE_FACTOR];
        System.arraycopy(next1, 0, next1Copy, 0, next1.length);
        next1 = next1Copy;

        int[] next2Copy = new int[next2.length * INCREASE_FACTOR];
        System.arraycopy(next2, 0, next2Copy, 0, next2.length);
        next2 = next2Copy;
    }
}
