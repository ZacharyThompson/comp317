package assignment3;

/**
 * A basic DoubleEndedQueue implementation.
 *
 * @param <E> The elements that will be contained in this ADT
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class DoubleEndedQueue<E> {
    /**
     * The head of the dequeue
     */
    private Node<E> head;

    /**
     * The tail of the dequeue
     */
    private Node<E> tail;

    /**
     * Peek at the head of the dequeue
     *
     * @return The element at the top of the dequeue.
     */
    public E peek() {
        return (head == null) ? null : head.element;
    }

    /**
     * Pushes an element onto the dequeue.
     * This is implemented by replacing the head with a node
     * that contains the new element.
     *
     * @param element The element to push onto the dequeue.
     */
    public void push(E element) {
        // Null guard
        if (head == null)
            tail = head = new Node<>(element, null);
        else
            head = new Node<>(element, head);
    }

    /**
     * Pops an element from the dequeue.
     * This is implemented by replace the head with the next item in dequeue.
     *
     * @return The element at the top of the dequeue.
     */
    public E pop() {
        // Null guard
        if (head == null)
            return null;
        Node<E> node = head;
        head = head.next;
        return node.element;
    }

    /**
     * Enqueues an element onto the dequeue.
     * Implemented by linking the tail to a new node that contains the
     * element and setting the new data.
     *
     * @param element The element to enqueue onto the dequeue.
     */
    public void enqueue(E element) {
        if (head == null) {
            tail = head = new Node<>(element, null);
        } else {
            tail.next = new Node<>(element, null);
            tail = tail.next;
        }
    }

    /**
     * A simple node structure that contains an element and a link to the next element.
     *
     * @param <E> The elements that are contained in this ADT.
     */
    private static class Node<E> {
        /**
         * The element the node represents.
         */
        private E element;

        /**
         * THe next node in the ADT.
         */
        private Node<E> next;

        /**
         * Constructs a node for a given element,
         *
         * @param element The element the node should represent
         * @param next The node to link this node to.
         */
        private Node(E element, Node<E> next) {
            this.element = element;
            this.next = next;
        }
    }
}
