package assignment3;

import java.util.Random;
import java.util.regex.Pattern;

/**
 * A class used to Test the functionality of the regex implementation.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class TestPatterns {
	private static Random random = new Random(0);

    /**
     * Main entry point into the program.
     * Takes as input 3 arguments
     *   1. A series of characters as a String that corresponds to the characters that form the alphabet
     *   2. The standard regular expression
     *   3. The assignment regular expression
     *
     * The program will simulate a series of input and compare against both pattern matching implementations.
     * Where these patterns differ will allow us to determine if there is a bug in the implementation.
     *
     * @param args The arguments for the project.
     */
    public static void main (String args[]) {
        if (args.length != 3) {
            System.err.println("Invalid arguments: Expected 'java TestPatterns <Alphabet> <Standard Regex Pattern> <Assignment Regex Pattern>");
            System.exit(0);
        }
        System.out.println(comparePatterns(args[0], args[1], args[2]));
    }

    /**
     * Compares a default regular expression and an assignment regular expression against other, attempting
     * to find input where these two do not match.
     *
     * @param regexA The default regular expression.
     * @param regexB The assignment regular expression.
     * @param alphabet The alphabet that the regular expressions will encompass.
     * @return Whether the two patterns match for all input.
     */
    public static boolean comparePatterns(String regexA, String regexB, String alphabet) {
        Pattern pattern = Pattern.compile(regexA);
		REcompile compile = new REcompile(regexB);
		REsearch search = new REsearch(compile);
        
        for (int i = 0; i < 100000; i++) {
            String s = genString(alphabet, random.nextInt(50));
			boolean matchA = pattern.matcher(s).find();
			int matchB = search.search(s);
            if (matchA != matchB >= 0) {
				System.out.println(s +  " : " + regexA + " - " + matchA + " : " + regexB + " - " + matchB);
				compile.getFiniteStateMachine().print(System.out);
                return false;
            }
        }
		return true;
    }

    /**
     * Generates a String of a defined size from a provided alphabet.
     *
     * @param alphabet The alphabet used to construct the String.
     * @param size The size of the String to be created.
     * @return The String generated.
     */
    private static String genString(String alphabet, int size) {
        StringBuilder builder = new StringBuilder();
        for (; size > 0; size--)
            builder.append(alphabet.charAt(random.nextInt(alphabet.length())));
        return builder.toString();
    }
}
