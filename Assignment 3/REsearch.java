package assignment3;

import java.io.*;

import static assignment3.FiniteStateMachine.BRANCH;
import static assignment3.FiniteStateMachine.NEGATE;
import static assignment3.FiniteStateMachine.WILDCARD;

/**
 * A regex searcher that can be used to match a String against a regular expression pattern
 * by using the FiniteStateMachine that has been built.
 *
 * @author Stephanie Pryor (1240480)
 * @author Zachary Thompson (1287280)
 */
public class REsearch {
    /**
     * The integer associated with the SCAN Object.
     */
    private static int SCAN = -1;

    /**
     * The FiniteStateMachine.
     */
    private FiniteStateMachine fsm;

    /**
     * Default constructor.
     *
     * Builds a FiniteStateMachine from input provided through an InputStream.
     * Each line of input is expected to take the form of a Single line that contains
     * the following separated by a single space.
     *  1. The state number the line represents
     *  2. The character the state attempts to match
     *  3. The first next state
     *  4. The second next state
     *
     *  It is therefore expected that each line of input have the form
     *  "%d %c %d %d", of which may be parsed using a predefined FiniteStateMachine.
     *
     * @param inputStream The InputStream to read input from.
     */
    public REsearch(InputStream inputStream) {
        build(inputStream);
    }

    /**
     * Constructs a REsearch Object from a predefined FiniteStateMachine
     *
     * @param fsm The FiniteStateMachine to associated with this Object.
     */
    public REsearch(FiniteStateMachine fsm) {
        this.fsm = fsm;
    }

    /**
     * Constructs a REsearch Object from a compiled regular expression.
     *
     * @param compile The REcompile instance to obtain a defined FiniteStateMachine
     *                from.
     */
    public REsearch(REcompile compile) {
        this.fsm = compile.getFiniteStateMachine();
    }

    /**
     * Builds a FiniteStateMachine from an InputStream.
     *
     * @param inputStream The InputStream to read input, by line, from.
     */
    private void build(InputStream inputStream) {
        // A regular expression compiler and search pair that will match any line that forms
        // a valid regular expression.
        REcompile validateRegex = new REcompile("[0123456789]+ . [0123456789]+ [0123456789]+");
        REsearch validateSearch = new REsearch(validateRegex);
        // A regular expression compiler and search pair that will match any segment to help
        // separate the input into the components.
        REcompile segmentRegex = new REcompile(" .![ ]!*");
        REsearch segmentSearch = new REsearch(segmentRegex);

        // Instantiate the new FiniteStateMachine
        fsm = new FiniteStateMachine(20);

        // Standard read by line pattern
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line, s, c, n1, n2;
            int index;
            while ((line = bufferedReader.readLine()) != null) {
                // If the pattern does not match, input was input
				if (validateSearch.search(line) == -1) {
				    System.err.println(String.format(
				            "The input was not valid : [%s]",
                            line)
                    );
				    System.exit(0);
                }

				// Otherwise split into the various components.
                index = segmentSearch.search(line);
                s = line.substring(0, index);
                c = line.substring(index + 1, index + 2);
                line = line.substring(index + 3);
                index = segmentSearch.search(line);
                n1 = line.substring(0, index);
                n2 = line.substring(index + 1);

                // As the original line passed an initial inspection, define the state in the
                // FiniteStateMachine.
                fsm.setState(
                        Integer.parseInt(s),
                        c.charAt(0),
                        Integer.parseInt(n1),
                        Integer.parseInt(n2)
                );
                fsm.state++;
            }
        } catch (IOException e) {
            System.err.println("An exception occurred while attempting to read from the InputStream");
        } catch (NumberFormatException e) {
            // This SHOULD only ever happen on Integer overflow.
            System.err.println("An exception occurred while attempting to parse to an Integer");
        }
    }

    /**
     * Searches an input for the index of the first match, returning -1 if
     * no match was found.
     *
     * @param input The string to match the FiniteStateMachine against.
     * @return The index of the start of the first match.
     */
    public int search(String input) {
        // Guard against null pointer
        if (fsm == null)
            throw new NullPointerException();

        // Start from the start and move along.
        for (int i = 0; i <= input.length(); i++) {
            if (search(input, i))
				return i;
		}
		// No match was found, so return -1
        return -1;
    }

    /**
     * Searches an input starting at a given position for an immediate match.
     * This method does not move along the method.
     *
     * @param input The String to match against.
     * @param start The index to start in the String.
     * @return Whether an immediate match was found.
     */
    private boolean search(String input, int start) {
        // Instantiate the DoubleEndedQueue with the start of the FiniteStateMachine
        // and a SCAN token.
        DoubleEndedQueue<Integer> deque = new DoubleEndedQueue<>();
        deque.push(SCAN);
        deque.push(0);

        // While the top of the dequeue is not a scan token.
        while (deque.peek() != SCAN) {
            // Process the current states. If one of those states
            // if the finish state, return immediately.
            if (processCurrentStates(deque, input, start))
                return true;
            // Consume a character and rotate the SCAN token
            start++;
            deque.enqueue(SCAN);
        }

        // No match must have been found.
        return false;
    }

    /**
     * Processes the current states of the DoubleEndedQueue, matching against a character at an index
     * in a given String.
     *
     * @param deque The queue that holds the current states and next states.
     * @param string The String to match against.
     * @param index The index in the String to perform character comparison.
     * @return Whether the final state was ever encountered.
     */
    private boolean processCurrentStates(DoubleEndedQueue<Integer> deque, String string, int index) {
        int state;
        // Iterate while the SCAN token has not been encountered.
        while ((state = deque.pop()) != SCAN) {
            // If the state is the final state, return true immediately.
            if (state == fsm.state) 
				return true;
            // If the character is a NEGATE token, toggle the NEGATE mode.
			if (fsm.ch(state) == NEGATE) {
				fsm.consume = !fsm.consume;
				pushState(deque, state);
			}
			// If the character is a BRANCH token, branch to the two states.
			else if (fsm.ch(state) == BRANCH) {
				pushState(deque, state);
			}
			// Otherwise ensure there exist a character in the String to match against.
			else if (index < string.length()) {
				char c = string.charAt(index);
				// If the character is a WILDCARD, the match was successful.
				if (fsm.ch(state) == WILDCARD) {
					enqueueState(deque, state);
				}
				// If consuming, ensure the characters match.
				else if (fsm.consume && c == fsm.ch(state)) {
					enqueueState(deque, state);
				}
				// If not consuming, ensure the characters do not match.
				else if (!fsm.consume && c != fsm.ch(state)) {
					pushState(deque, state);
				}
			}
        }
        // The final state was not encountered.
        return false;
    }

    /**
     * Pushes one or two states to the top of a DoubleEndedQueue.
     *
     * @param deque The DoubleEndedQueue to push the states to.
     * @param state The state to move along from.
     */
    private void pushState(DoubleEndedQueue<Integer> deque, int state) {
        if (fsm.next1(state) != fsm.next2(state))
            deque.push(fsm.next1(state));
        deque.push(fsm.next2(state));
    }

    /**
     * Enqueues one or two states to the bottom of a DoubleEndedQueue.
     *
     * @param deque The DoubleEndedQueue to enqueue the states to.
     * @param state The state to move along from.
     */
    private void enqueueState(DoubleEndedQueue<Integer> deque, int state) {
        if (fsm.next1(state) != fsm.next2(state))
            deque.enqueue(fsm.next1(state));
        deque.enqueue(fsm.next2(state));
    }

    /**
     * Searches an InputStream for matches and printed to a given OutputStream.
     *
     * Each line is treated as a distinct entity, and if it contains a match
     * it will be printed to the given OutputStream
     *
     * @param inputStream The InputStream to read input from.
     * @param outputStream The OutputStream to write the matches to.
     */
    public void search(InputStream inputStream, PrintStream outputStream) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (search(line) >= 0)
                    outputStream.println(line);
            }
        } catch (IOException e) {
            // The exception can only come from the Input (PrintStreams swallow)
            System.err.println("An exception occurred when attempting to read");
        }
    }

    /**
     * Main entry point into the searcher.
     * Takes as input a file name for which Text input will be read and compared against
     * a FiniteStateMachine that was produced for a regular expression.
     *
     * Note that the program will read from Standard in the state description for the
     * FiniteStateMachine and only perform analysis once the EOF has been received.
     *
     * @param args The program arguments
     */
    public static void main (String args[]) {
        // Ensure the arguments are valid.
        if (args.length != 1) {
            System.err.println("Invalid arguments: Expected 'java REsearch <File Name>'");
            System.exit(0);
        }
        // Ensure the file exists.
        File file = new File(args[0]);
        if (!file.exists()) {
            System.err.println(String.format("Invalid arguments: %s does not exist", args[0]));
            System.exit(0);
        }
        // Construct a REsearch Object.
        REsearch search = new REsearch(System.in);
        try {
            // Search a file for any matches and print to Standard out.
            search.search(new FileInputStream(file), System.out);
        } catch (FileNotFoundException e) {
            System.err.println("An error occured while attempting to read from the file");
        }
    }
}
